# __super2__
#
#
## Super 2 Game Rules:

#### General Rules:
1. Players start off with 26 cards
2. Players will have 7 cards in their hand at all times
3. When a player puts down x cards, the player must pick up x cards from the rest of their deck. Exception: If there are y cards in the deck remaining and y<x, the player must pick up all cards remaining in the deck.
4. If it is a player’s turn if they have only 2 options:
    4.1. The player starts off with a listed pattern
    4.2. The player must continue the pattern that has been placed down with a higher “pattern”
        4.2.1. Single (Higher value first, then determine the highest suit)
            4.2.1.1. Case: 7C>3H ; 2D > 2C
        4.2.2. Pair (highest single in a pair must beat the highest single in the other pair)
            4.2.2.1. Case: (3C,3D) > (3S,3H) ; (3S, 3D) > (3H, 3C) ; (3H, 3D) > (3S, 3C) ; (4S, 4H) > (3D, 3S) 
        4.2.3. Triple (highest single in triple must beat the other highest single in triple)
            4.2.3.1. Case: (7C,7D,7H)>(3H,3C,3D) ; (2D,2H,2C) > (AC,AH,AS)
        4.2.4. Full House (Highest triple in a full house must beat the other highest triple in the other full house)
            4.2.3.1. Case: (10C,10H,10D,3S,3D) > (5H,5D,5C,KC,KS) ; (2D) > (2C)
        4.2.5. Straight (3-10) (highest single in a straight must beat the highest single in the other straight)
            4.2.5.1. Case: (3X, 4X, 5D) > (3X, 4X, 5X), (QX, KX, AD) > (QX, KX, 5X)
        4.2.6. Straight Pair (3-5) (highest single in a straight must beat the highest single in the other straight)
            4.2.6.1. Case: (5X,5X,6X,6X,7X,7D) > (5X,5X,6X,6X,7X,7C) ; (JX,JX,QX,QX,KS,KH) > (10X,10X,JX,JX,QH,QD)
        4.2.7. Straight Triple (3) (highest single in a straight must beat the highest single in the other straight)
            4.2.7.1 Case: (5X,5X,5X,6X,6X,6X,7H,7S,7D) > (5X,5X,5X,6X,6X,6X,7H,7S,7C) ; (JX,JX,JX,QX,QX,QX,KH,KS,KC) > (10X,10X,10X,JX,JX,JX,QS,QH,QD)
5. A “pattern” is finished when player A cannot beat player B’s pattern. In that case, player A must pass and player B gets to start a new pattern.
6. A game is considered finished when either player A or player B has 0 cards left.
7. A player by default will pass if they do not make a decision within (15) seconds.
#
#### The Order of Cards/Face/Suits (lowest to highest):
#
| | __Lowest__ | | | | | | | | | | | | __Highest__ |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
|__Lowest__|3S|4S|5S|6S|7S|8S|9S|10S|JS|QS|KS|AS|2S|
||3H|4H|5H|6H|7H|8H|9H|10H|JH|QH|KH|AH|2H|
||3C|4C|5C|6C|7C|8C|9C|10C|JC|QC|KC|AC|2C|
|__Highest__|3D|4D|5D|6D|7D|8D|9D|10D|JD|QD|KD|AD|__2D__|
#
#### Patterns player can put down:
* Single
    * Example: 2C
* Pair
    * Example: 2S , 2H
* Triple
    * Example: 2S, 2H, 2C
* Quad (Bomb)
    * Example: 2S, 2H, 2C, 2D
* Full House
    * Example: 2S,2H,2C,AS,AH
* Straight (3-7)
    * Straights cannot have “2” included
    * Straights need at least 3 consecutive
    * Example: QC, KH, AC
* Straight Pair (3)
    * Straight pairs cannot have “2” included
    * Straights need at least 3 consecutive
    * Example: QH,QS,KD,KS,AC,AD
* Straight Triple (3)
    * Straight triple cannot have “2” included
    * Straights need at least 6 consecutive
    * Example: QH,QS,QC,KC,KD,KS,AC,AD,AH








