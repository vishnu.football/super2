var menu = document.querySelectorAll('.menu');

menu.forEach(function (m) {
	m.addEventListener('click', function (evt) {
		if (m.classList.contains('active')) {
			m.classList.add('clicked');
			m.classList.remove('active');
		} else
		{
			m.classList.remove('clicked');
			m.classList.add('active');
		}
	});
});