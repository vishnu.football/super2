Issues Dealt with in converting to html
Collections (sort and shuffle)
Comparable
Display Width/Height
System.out (just println is fine)
Sublist
Stack

--------
I replaced Stack<List<Card>> with List<List<Card>>, and used add(0,selection) instead of push(selection) and get(0) instead of peek()
I replaced sublist with for loop
I replaced display width and height with dwidth and dheight which I preset to 1366, 768 but intend to eventually make it specific to the screen of the user
I replaced collections sort and shuffle with a sortList and shuffleList method. The sort is a bubble sort and the shuffle utilizes math.random
I removed Comparable, since collections is no longer used
I replaced System.out.println with just "println"
I had a font issue (on the html), tried to fix it with a font file but it doesn't seem to be fixed
Code works as intended now.

Also I replaced the hash map with an if-else statement (which is actually simpler looking and more efficient)
