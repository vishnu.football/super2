import java.util.*;
Card button;
String A_id,B_id,curr_id;
Hand p1Hand, p2Hand, A_hand,B_hand,theHand,currhand;
TableDeck myTableDeck;
Deck myDeck;
int turn, dWidth, dHeight;
PImage img;
PFont displayFont, cardFont;

void setup()
{
  
  dWidth = 1366;
  dHeight = 768;
  println("displayWidth" + dWidth + "displayHeight" + dHeight);
  size(1366,768);
  frameRate(60);

  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  imageMode(CENTER);
  displayFont = loadFont("LucidaSans-48.vlw");
    //  displayFont = createFont("andalemo",3*dWidth/100);

  //img = loadImage("back.png");
  String p1_id = "ABCDEF123";
  String p2_id = "UVWXYZ789";
  myDeck = new Deck(p1_id,p2_id);
  myTableDeck = new TableDeck();
  p1Hand = new Hand(myDeck, p1_id);
  p2Hand = new Hand(myDeck, p2_id);
   turn = 1;
   if(p1Hand.getHand().get(0).getRank() < p2Hand.getHand().get(0).getRank()){
      //p1 starts
      println("P1 Starts");
      A_id = p1_id;
      B_id = p2_id;
      A_hand = p1Hand;
      B_hand = p2Hand;
    }
    else{
      //p2 starts
      A_id = p2_id;
      B_id = p1_id;
      A_hand = p2Hand;
      B_hand = p1Hand;
    }
    
   button = new Card(19, 0, 1); button.setxPos(4*dWidth/6); button.setyPos(3*dHeight/6); button.setWidth(dWidth/20); button.setHeight(7*dWidth/100); button.setSpeed(50);
 }

void mouseClicked()
{
  if(mouseY>2*dHeight/6)
  {
    theHand = A_hand;
  }
  else
  {
    theHand = B_hand;
  }
   for (int i = 0; i < theHand.getHand().size(); i++) {
 if (mouseX > (theHand.getHand().get(i).getxPos() - theHand.getHand().get(i).getWidth() / 2) && mouseX < (theHand.getHand().get(i).getxPos() + theHand.getHand().get(i).getWidth() / 2) && mouseY > (theHand.getHand().get(i).getyPos() - theHand.getHand().get(i).getHeight() / 2) && mouseY < (theHand.getHand().get(i).getyPos() + theHand.getHand().get(i).getHeight() / 2)) {
    if(theHand.getHand().get(i).getClicked()==true)
      {theHand.getHand().get(i).setClicked(false);}
      else
      {theHand.getHand().get(i).setClicked(true);}

     }
   }
  
   if(mouseX > (button.getxPos()-button.getWidth()/2) && mouseX < (button.getxPos() + button.getWidth() / 2) && mouseY > (button.getyPos() - button.getHeight() / 2) && mouseY < (button.getyPos() + button.getHeight() / 2))
   {
      if(button.getClicked()==true)
      {button.setClicked(false);}
      else
      {button.setClicked(true);}
   }
   
}

void draw(){
  background(color(0, 50, 0));

   textFont(displayFont);
   textSize(3*dWidth/100);
   fill(255,255,255);
     
  if(turn%2==1)
  {curr_id = A_id; currhand = A_hand;  text("Player A's Turn", 3*dWidth/4, 3*dHeight/4);}
  else
  {curr_id = B_id; currhand = B_hand;  text("Player B's Turn", 3*dWidth/4, 3*dHeight/4);}
  button.display();
  if(!(curr_id==A_id && turn == 1))
   {
  myTableDeck.displayStack();
   }
  A_hand.displayHand("A");
  B_hand.displayHand("B");
  
  if(!(A_hand.getHand().isEmpty() || B_hand.getHand().isEmpty())&&!isDraw(myTableDeck)){
    if(button.getClicked()==true) //wait until button is clicked
      {
        myTableDeck.turn(curr_id, currhand.select(curr_id),myDeck,currhand,1,turn);
        println("You Pressed The Button");
             turn++;
                button.setClicked(false);
                if(turn%2==1)
                {button.setValue(button.getValue()-1);}
        }

      }
      
  else
  {
    if(A_hand.getHand().isEmpty()){
      textSize(8*dWidth/100);
      fill(255,255,255);
      text("Player A Won!", dWidth/2, dHeight/2);
    }

    else if(B_hand.getHand().isEmpty()){
      textSize(8*dWidth/100);
      fill(255,255,255);
      text("Player B Won!", dWidth/2, dHeight/2);
    }
    else if(isDraw(myTableDeck)) {
      textSize(8*dWidth/100);
      fill(255,255,255);
      text("Its a Draw!", dWidth/2, dHeight/2);
    }
    else {
      println("Something went wrong");
    }
  }
  }
 


class Card{
  private float xPos, yPos, width, height, speed;
  private color rgb, rgbstroke;
  private boolean clicked;
  private int value, suit, rank;
  Card(int value, int suit, int rank)
  {
    this.value = value;
    this.suit = suit;
    this.rank = rank;
    this.clicked = false;
  }
  public String getStringValue(){
    switch(this.value){
    case 11:
      return "J";
    case 12:
      return "Q";
    case 13:
      return "K";
    case 14:
      return "A";
    case 15:
      return "2";
    default:
      return ""+this.value;
    }
  
  }
  public String getStringSuit(){
    switch(this.suit){
    case 1:
      return "S";
    case 2:
      return "H";
    case 3: 
      return "C";
    case 4:
      return "D";
    case 0:
      return "Back";
    default:
      return "X";
    }
    
  }
  public int getRank()
  {return this.rank;}
  
  public int getValue()
  {return this.value;}
  
  public int getSuit()
  {return this.suit;}
  
  public float getxPos()
  {return this.xPos;}
  
  public float getyPos()
  {return this.yPos;}
  
  public float getWidth()
  {return this.width;}
  
  public float getHeight()
  {return this.height;}
  
  public float getSpeed()
  {return this.speed;}
  
  public color getRGB()
  {return this.rgb;}
   
  public boolean getClicked()
  {return this.clicked;}

  public void moveX(float s)
  {this.xPos -= s;}
 
  public void setValue(int value){
    this.value = value;
  }
  public void setSuit(int suit){
    this.suit = suit;
  }
  public void setRank(int rank){
    this.rank = rank;
  }
  
  public void setxPos(float xPos)
  {this.xPos = xPos;}
  
  public void setyPos(float yPos)
  {this.yPos = yPos;}
    
  public void setWidth(float width)
  {this.width = width;}
   
  public void setHeight(float height)
  {this.height = height;}
   
  public void setSpeed(float speed)
  {this.speed = speed;}
   
  public void setStroke(color rgbstroke)
  {this.rgbstroke = rgbstroke;}
   
  public void setClicked(boolean click)
  {this.clicked = click;}
  
  //@Override
  //public int compareTo(Card compareCard) {
  //  // TODO Auto-generated method stub
  //  int compareRank = compareCard.getRank();
  //  //ascending order
  //  return this.rank - compareRank;
  //}
  
  public void display(){

    if(this.suit!=(0))
    {
      fill(color(255,255,255));
      strokeWeight(3); 
      stroke(rgbstroke);
      rect(this.xPos, this.yPos, this.width, this.height);
      textSize(8*width/25);
      fill(rgb);
      if(this.getStringValue().equals("10")) //Special case for 10
      {
            text("I0", xPos-15*width/48, yPos-8*height/20);
            text("I0", xPos+17*width/48, yPos+7*height/20);
      }
      else if(this.getStringValue().equals("J")) //Special case for J
      {
           text(this.getStringValue(), xPos-17*width/48, yPos-8*height/20);
           text(this.getStringValue(), xPos+19*width/48, yPos+13*height/40);
      }
      else if(this.getStringValue().equals("Q")) //Special case for Q
      {
           text(this.getStringValue(), xPos-15*width/48, yPos-8*height/20);
           text(this.getStringValue(), xPos+16*width/48, yPos+13*height/40);
      }
      else{
      text(this.getStringValue(), xPos-17*width/48, yPos-8*height/20);
      text(this.getStringValue(), xPos+9*width/24, yPos+7*height/20);
      }
    }
    else
    {
      if(this.rank==1)
      {
        this.SelectButton();
      }
      else
      {
      this.Back();
      }
    }
    if(this.getStringSuit().equals("H"))
    {this.Heart(); rgb=color(255,0,0);}   
    else if(this.getStringSuit().equals("D"))
    {this.Diamond(); rgb=color(255,0,0); } 
    else if(this.getStringSuit().equals("S"))
    {this.Spade(); rgb=color(0,0,0); } 
    else if(this.getStringSuit().equals("C"))
    {this.Club(); rgb=color(0,0,0); } 
    
  }
   
   void Heart()
   {
      smooth();
      noStroke();
      beginShape();
      fill(rgb);
      vertex(xPos, yPos); 
      bezierVertex(xPos, yPos-2*height/7, xPos+37*width/50, yPos-1*height/7, xPos, yPos+2*height/7); 
      vertex(xPos, yPos+2*height/7); 
      bezierVertex(xPos-37*width/50, yPos-1*height/7,xPos, yPos-2*height/7, xPos, yPos); 
      endShape(); 
   }
   void Diamond()
   {
      noStroke();
      fill(rgb);
      quad(xPos, yPos+2*height/7, xPos+15*width/50, yPos, xPos, yPos-2*height/7, xPos-15*width/50, yPos);
   }
   void Spade()
   {
      smooth();
      noStroke();
      fill(rgb);
      beginShape();
      vertex(xPos, yPos+height/17); 
      bezierVertex(xPos, yPos+2*height/7, xPos+37*width/50, yPos+3*height/14, xPos, yPos-3*height/17); 
      vertex(xPos, yPos-3*height/17); 
      bezierVertex(xPos-37*width/50, yPos+3*height/14, xPos, yPos+2*height/7, xPos, yPos+height/17); 
      endShape(); 
      
      smooth();
     
      beginShape();   
      noStroke();
      fill(rgb);
      vertex(xPos-11*width/50, yPos+2*height/7); 
      bezierVertex(xPos-7*width/50, yPos+2*height/7, xPos, yPos+1*height/7, xPos, yPos ); 
      vertex(xPos, yPos); 
      bezierVertex(xPos, yPos+1*height/7, xPos+7*width/50, yPos+2*height/7, xPos+11*width/50, yPos+2*height/7); 
      vertex(xPos+11*width/50, yPos+2*height/7);
      bezierVertex(xPos,yPos+2*height/7,xPos,yPos+2*height/7,xPos-11*width/50,yPos+2*height/7);      
      endShape();      
   }
   
   void Club()
   {
      smooth();
      noStroke();
      fill(rgb);
      ellipse(xPos-17*width/100,yPos+3*height/56,26*height/100,26*height/100);
      ellipse(xPos+17*width/100,yPos+3*height/56,26*height/100,26*height/100);
      ellipse(xPos,yPos-11*height/100,26*height/100,26*height/100);
      
      smooth();
     
      beginShape();  
      noStroke();
      fill(rgb);
      vertex(xPos-11*width/50, yPos+2*height/7); 
      bezierVertex(xPos-7*width/50, yPos+2*height/7, xPos, yPos+1*height/7, xPos, yPos ); 
      vertex(xPos, yPos); 
      bezierVertex(xPos, yPos+1*height/7, xPos+7*width/50, yPos+2*height/7, xPos+11*width/50, yPos+2*height/7); 
      vertex(xPos+11*width/50, yPos+2*height/7);
      bezierVertex(xPos,yPos+2*height/7,xPos,yPos+2*height/7,xPos-11*width/50,yPos+2*height/7);      
      endShape();      
   }
   
   void Back()
   {
      //fill(rgb); //to be replaced with deck design
      noStroke();
      //image(img,xPos,yPos,dWidth/20+3,7*dWidth/100+3);
      fill(color(255,255,255));
      textSize(8*width/25);
      if(!this.getStringValue().equals("0"))
      {
      text(value,xPos,yPos);
      }
   }
   
   void SelectButton()
   {
      //fill(rgb); //to be replaced with deck design
      strokeWeight(3); 
      stroke(rgb);
      fill(color(255,255,255));
      rect(this.xPos, this.yPos, this.width, this.height);
      textSize(8*width/25);
      if(!this.getStringValue().equals("0"))
      {
      fill(color(0,0,0));
      text(value,xPos,yPos);
      }
   }
}


public class Deck{
    // Vars:
    private List<Card> deck;
    private List<Card> p1_deck;
    private List<Card> p2_deck;
    private String id1;
    private String id2;
    // Constructor:
    public Deck(String id1,String id2){
      this.id1 = id1;
      this.id2 = id2;
      List<Card> cards = new ArrayList<Card>();
      int k = 0;
      for(int i = 3;i<=15;i++){
        for(int j = 1;j<=4;j++){
          Card tempCard = new Card(i,j,k);
          cards.add(tempCard);
         k++;
        }
      }
      shuffleList(cards);  
      
      
        List<Card> p1 = new ArrayList<Card>();
        List<Card> p2 = new ArrayList<Card>();
        for (int i = 0; i<=25; i++)
        {
          p1.add(cards.get(i));
        }
        for (int i = 25; i<=51; i++)
        {
          p2.add(cards.get(i));
        }
        this.deck = cards;
      this.p1_deck = p1;
      this.p2_deck = p2;
    }
    
        
    // Methods:
    public boolean isEmpty(String player_id) {
      if(player_id.equals(this.id1)){
        return this.p1_deck.isEmpty();
      }else if(player_id.equals(this.id2)){
        return this.p2_deck.isEmpty();
      }else{
        //default case is empty
        return true;
      }
      
    }
    // return deck of input player_id
    public List<Card> getDeck(String player_id){
      if(player_id.equals(this.id1)){
        return p1_deck;
      }else if(player_id.equals(this.id2)){
        return p2_deck;
      }else{
        //default
        return deck;
      }  
    }
    
    // display the deck of input id
    
    public void sortWeight(List<Card> selection){
        sortList(selection);
    }
    // shuffle deck 
    public void shuffleDeck(String player_id){
      if(player_id.equals(id1)){
        shuffleList(p1_deck);
      }else if(player_id.equals(id2)){
        shuffleList(p2_deck);
      }else{
        shuffleList(deck);
        //default is full deck
      }
    }
    public void shuffleDeck(){
      shuffleList(deck);
    }

}
public class Hand{
    // Variables:
    private List<Card> hand = new ArrayList<Card>();
   
    // Constructor:
    public Hand(Deck deck,String player_id){
      this.hand = draw_card(deck,player_id,7);
      sortWeight(this.hand);
    }
    public Hand(Hand hand){
      this.hand = hand.hand;
    }
    // Methods:
    
    public List<Card> select(String player_number){
       List<Card> selection = new ArrayList<Card>();;
      List<Integer> intList = new ArrayList<Integer>();
       this.displayHand(player_number);
       for(int i = 0; i<this.hand.size(); i++)
        {
        if(this.hand.get(i).getClicked()==true)
          {
          intList.add(i);
          }
        }
       sortIntList(intList);
       for(int i = intList.size()-1; i>=0; i--)
        {
        selection.add(this.hand.get(intList.get(i)));
        }
      return selection;
    }
    public void sortWeight(List<Card> selection){
        sortList(selection);
    }
    public List<Card> draw_card(Deck deck,String player_id, int num_draws){
      List<Card> rethand = new ArrayList<Card>();
      if(deck.isEmpty(player_id)){
        return rethand;
      }
//      deck.displayDeck(player_id);
      for(int i=0;i<num_draws;i++){
        if(deck.isEmpty(player_id)) {
          break;
        }
        //draw num_draws times
        //get deck for player_id deck
        List<Card> tempdeck = deck.getDeck(player_id);
        rethand.add(tempdeck.get(tempdeck.size()-1));
        tempdeck.remove(tempdeck.size()-1);
      }
      
      return rethand;
    }
    // output cards in hand of current player in console
    public void displayHand(String player_number){
     for (int i = 0; i < hand.size(); i++) {
     this.getHand().get(i).setxPos((0.9 + 1.4 * i) * hand.get(i).getWidth());
          this.getHand().get(i).setWidth(dWidth/20);
          this.getHand().get(i).setHeight(7*dWidth/100);
          this.getHand().get(i).setSpeed(50);
          if(player_number.equals("A"))
        {
          this.getHand().get(i).setyPos(5*dHeight/6);
        }
        else
        {
          this.getHand().get(i).setyPos(1*dHeight/6);
        }
          hand.get(i).display();
          if (mouseX > (hand.get(i).getxPos() - hand.get(i).getWidth() / 2) && mouseX < (hand.get(i).getxPos() + hand.get(i).getWidth() / 2) && mouseY > (hand.get(i).getyPos() - hand.get(i).getHeight() / 2) && mouseY < (hand.get(i).getyPos() + hand.get(i).getHeight() / 2)) {
                      if(hand.get(i).getClicked()==true)
                       {
                         hand.get(i).setStroke(color(0, 0, 128));  //clicked and in region(making selection)
                       }
                       else
                       {
                         hand.get(i).setStroke(color(255,255,0));    //not clicked and in region(hover)
                       }
                  }
                  
                  else {   
                    if(hand.get(i).getClicked()==true)
                       {
                         hand.get(i).setStroke(color(0, 0, 128));  //clicked and not in region(selection preserved)
                       }
                    else
                       {
                         hand.get(i).setStroke(color(255,255,255));  //not clicked and not in region(normal card)
                       }
                  } 
            }
    }
    
    public List<Card> getHand(){
      return this.hand;
    }
  }

public class TableDeck {
  // Vars:
  //Stack<List<Card>> trash = new Stack<List<Card>>(); 
  List<List<Card>> trash = new ArrayList<List<Card>>(); 
  public TableDeck() {
    Card passcard = new Card(1,-1,1);
    List<Card> passlist = new ArrayList<Card>();
    passlist.add(passcard);
    this.trash.add(0,passlist);
    //this.trash.push(passlist);
  }
  
  // Methods:
  public List<List<Card>> getTrash(){
    return trash;
  }
  public void displayStack(){
    if(!trash.isEmpty()) {
      displayDeck(trash.get(0));
    }
  }

  public void displayDeck(List<Card> deck){
    for (int i = 0; i < deck.size(); i++) {
        deck.get(i).setxPos((0.9 + 1.4 * i) * deck.get(i).getWidth());
        deck.get(i).setWidth(dWidth/20);
        deck.get(i).setHeight(7*dWidth/100);
        deck.get(i).setSpeed(50);
        deck.get(i).setyPos(1*dHeight/2);
        deck.get(i).display();  
     }    
    }
public void turn(String player_id,List<Card> selection, Deck deck, Hand currhand, int player_num, int turn_num){
      sortWeight(selection);          
      List<Card> topCards = trash.get(0);
      //List<Card> topCards = trash.peek();
      if(player_num == 1 && turn_num == 1) {
        if(!selection.contains(currhand.getHand().get(0)) || getHandType(selection).equals("Invalid") || selection.isEmpty()){
          println("As first player, your selection needs to include your lowest card, not a pass, and needs to be valid pattern.");
          turn --;
          selection = currhand.select(player_id);
        }else{
          trash.add(0,selection);
           //trash.push(selection);
          Hand temphand1 = new Hand(currhand);
          for(Card card: selection) temphand1.getHand().remove(card);
          temphand1.sortWeight(temphand1.getHand());
          currhand = temphand1;
          currhand.getHand().addAll(currhand.draw_card(deck, player_id, selection.size()));
          sortWeight(currhand.getHand());
        }
      }else if(selection.isEmpty()) {
        //empty selection (valid pass)
        Card passcard = new Card(player_num,-1,turn_num);
        List<Card> passlist = new ArrayList<Card>();
        passlist.add(passcard);
        //trash.push(passlist);
        trash.add(0,passlist);
        println("You Passed.");
      }else if((isHigherSelection(topCards,selection) || getHandType(topCards).equals("Pass")) && !getHandType(selection).equals("Invalid")) {
        //selection validation
        //put down selection to trash
        //trash.push(selection);
        trash.add(0,selection);
        //remove card from hand
        Hand temphand1 = new Hand(currhand);
        for(Card card: selection) temphand1.getHand().remove(card);
        temphand1.sortWeight(temphand1.getHand());
        currhand = temphand1;
        //draw new cards
        currhand.getHand().addAll(currhand.draw_card(deck, player_id, selection.size()));
        sortWeight(currhand.getHand());
      }else{
        println("Invalid selection. Please try again.");
        this.displayStack();
        turn --;
        selection = currhand.select(player_id);
      }
     for(Card card: currhand.getHand()) {card.setClicked(false);}

  }
  public void displaySelection(List<Card> selection) {
    String tempString = "Current Selection: [ ";
    for(int i=0 ;i<selection.size();i++){ 
      
      
      tempString += selection.get(i).getStringValue() + "" + selection.get(i).getStringSuit() + " "; 
    }
    tempString += "]";
    println(tempString);
  }
  public boolean isHigherRank(Card card1, Card card2){
    if(card1.getRank() < card2.getRank()){
      //card2 is higher than card1
      return true;
    }else{
      return false;
    }
  }
  public boolean isHigherSelection(List<Card> topCards,List<Card> selection){
    //compare which cards are higher weight
    
    //bomb case
    if(getHandType(selection).equals("Bomb")){
      if(getHandType(selection).equals(getHandType(topCards))){
        if(topCards.get(0).getRank() < selection.get(0).getRank()){
          return true;
        }else{
          return false;
        }
      }else{
        return true;
      }
    }
    //other cases, check if hand types match
    if(getHandType(selection).equals(getHandType(topCards)))
    {
      switch (getHandType(selection)){
        case "Single":
          return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
        case "Pair":
          return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
        case "Triple":
          return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
        case "FullHouse":
          return (isHigherRank(topCards.get(2),selection.get(2))); //the middle is always the triple
        case "Straight3":
          return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
        case "Straight4":
          return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
        case "Straight5":
          return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
        case "Straight6":
          return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
        case "Straight7":
          return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
        case "Invalid":
          return false;
        default:
          return false;
      }
    }
    
    //no match in hand type
    return false;
  }
  
  public void sortWeight(List<Card> selection){
      sortList(selection);
  }
  
  
  public String getHandType(List<Card> selection){
    if(selection.size() == 1)
    {
      if(selection.get(0).getSuit() == -1){
        return "Pass";
      }else{
        return "Single";
      }
      
    }
    else if(selection.size() == 2)
    {
      if(isPair(selection)){
        return "Pair";
      }
      else{
        return "Invalid";
      }
    }
    else if(selection.size() == 3)
    {
      if(isTriple(selection)){
        return "Triple";
      }
      else
      {
        if(isStraight(selection))
        {
          return "Straight3";
        }
        
        else{
          return "Invalid";
        }
      }
      
    }
    else if(selection.size() == 4)
    {
      if(isQuad(selection)){
        return "Bomb";
      }
      else
      {
        if(isStraight(selection))
        {
          return "Straight4";
        }
        
        else{
          return "Invalid";
        }
      }
      
    }
    
    else if(selection.size() == 5)
    {
      
      if(isFullHouse(selection)){
        return "FullHouse";
      }
      else
      {
        
        if(isStraight(selection))
        {
          return "Straight5";
        }
        
        else{
          return "Invalid";
        }
      }
      
    }
    
    else if(selection.size() == 6)
    {
        
        if(isStraight(selection))
        {
          return "Straight6";
        }
        
        else{
          return "Invalid";
        }
    }
    
    else if(selection.size() == 7)
    {
        if(isStraight(selection))
        {
          return "Straight7";
        }
        
        
        else{
          return "Invalid";
        }
    }
    
    
    else
    {
      return "Invalid";
    }
    
  }
  
  public boolean isStraight(List<Card> selection)
  {   
    int size = selection.size();
    if(selection.get(size-1).getValue() == 15)
    {
      return false;
    }
    else
    {
      for(int i = 0; i<size-1; i++)
      {
        if(selection.get(i).getValue() != (selection.get(i+1).getValue()-1))
        {
          return false;
        }
          
      }
      return true;
    }    
  }
  
  public boolean isPair(List<Card> selection)
  {
    if(selection.get(0).getValue() == selection.get(1).getValue())
    {
      return true;
    }
    return false;
  }
  
  public boolean isTriple(List<Card> selection)
  {
    if(selection.get(0).getValue() == selection.get(1).getValue() && selection.get(1).getValue() == selection.get(2).getValue()){
      return true;
    }
    return false;
  }
  public boolean isQuad(List<Card> selection)
  {
    if(selection.get(0).getValue() == selection.get(1).getValue() && selection.get(1).getValue() == selection.get(2).getValue() && selection.get(2).getValue() == selection.get(3).getValue())
    {
      return true;
    }
    return false;
  }
  public boolean isFullHouse(List<Card> selection){
    if(selection.get(0).getValue() == selection.get(1).getValue() && selection.get(3).getValue() == selection.get(4).getValue())// if the first 2 and the last 2 are the same
    {
      if(selection.get(2).getValue() == selection.get(1).getValue()||selection.get(2).getValue() == selection.get(3).getValue())// if the middle is equal to either the first 2 or last 2
      {
        return true; 
      }
      else
      {
        return false;
      }
    }
    return false;
  }
}

public static boolean isDraw(TableDeck TD)
  {
    int s = TD.getTrash().size();
    if(s >= 6){
      for(int i=s-6 ; i<s; i++)
      {
        if(TD.getTrash().get(i).get(0).getSuit() != -1)
        {
          return false;
        }
      }
      return true;
    }
    else {
      return false;
    }
    
  }
  
void shuffleList(List <Card> MyCards) {
    for (int i = MyCards.size() - 1; i > 0; i--) {
        int j = (int)Math.floor(Math.random() * (i + 1));
        Card tempCard = MyCards.get(i);
        MyCards.set(i,MyCards.get(j));
        MyCards.set(j,tempCard);
    }
}

void sortList(List <Card> MyCards) { // bubble sort

        int n = MyCards.size(); 
        for (int i = 0; i < n-1; i++) 
        {
            for (int j = 0; j < n-i-1; j++) 
            {
                if (MyCards.get(j).getRank() > MyCards.get(j+1).getRank()) 
                { 
                    // swap arr[j+1] and arr[i] 
                    Card temp = MyCards.get(j); 
                    MyCards.set(j,MyCards.get(j+1)); 
                    MyCards.set(j+1,temp); 
                } 
            }
        }
}

void sortIntList(List <Integer> MyIntegers){
  int n = MyIntegers.size();
   for (int i = 0; i < n-1; i++) 
        {
            for (int j = 0; j < n-i-1; j++) 
            {
                if (MyIntegers.get(j) > MyIntegers.get(j+1)) 
                { 
                    // swap arr[j+1] and arr[i] 
                    int temp = MyIntegers.get(j); 
                    MyIntegers.set(j,MyIntegers.get(j+1)); 
                    MyIntegers.set(j+1,temp); 
                } 
            }
        }
}
