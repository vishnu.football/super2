import java.util.*;


public class TableDeck {
	// Vars:
	Stack<List<Card>> trash = new Stack<List<Card>>(); 
	public TableDeck() {
		// TODO Auto-generated constructor stub
		Card passcard = new Card(-1,-1,-1);
		List<Card> passlist = new ArrayList<Card>();
		passlist.add(passcard);
		this.trash.push(passlist);
	}
	
	// Methods:
	public Stack<List<Card>> getTrash(){
		return trash;
	}
	public void displayStack(){
//		List<List<Card>> temp_trash = Arrays.toString(this.trash.toArray());
		System.out.println("==========");
		System.out.println("Table Deck:");
		if(trash.isEmpty()) {
			System.out.println("[ ]");
		}
		else {
			displayDeck(trash.get(trash.size()-1));
		}
		
		/*
		for(int i=0;i<trash.size();i++){
			displayDeck(trash.get(i));
		}
		*/
		System.out.println("==========");
	}

	public void displayDeck(List<Card> deck){
		String tempString = "[ ";
		//tempString += deck.get(deck.size()-1).getStringValue() + "" + deck.get(deck.size()-1).getStringSuit() + " ";
		
		for(int i=0 ;i<deck.size();i++){ 
			
			tempString += deck.get(i).getStringValue() + "" + deck.get(i).getStringSuit() + " "; 
		}
		
		tempString += "]";
		
		System.out.println(tempString);
	}
	public void turn(String player_id,List<Card> selection, Deck deck, Hand currhand, int player_num, int turn_num){
		
		while(true){
			sortWeight(selection);					
			List<Card> topCards = trash.peek();
			if(player_num == 1 && turn_num == 1) {
				//first player condition
				if(!selection.contains(currhand.getHand().get(0)) || getHandType(selection).equals("Invalid") || selection.isEmpty()){
					//invalid first player selection
					System.out.println("As first player, your selection needs to include your lowest card, not a pass, and needs to be valid pattern.");
					selection = currhand.select(player_id);
				}else{
					//valid first player selection
					//put down selection to trash
					trash.push(selection);
					//remove card from hand
					Hand temphand1 = new Hand(currhand);
					for(Card card: selection) temphand1.getHand().remove(card);
					temphand1.sortWeight(temphand1.getHand());
					currhand = temphand1;
					//draw new cards
					currhand.getHand().addAll(currhand.draw(deck, player_id, selection.size()));
					sortWeight(currhand.getHand());
					break;
				}
			}else if(selection.isEmpty()) {
				//empty selection (valid pass)
				Card passcard = new Card(player_num,-1,turn_num);
				List<Card> passlist = new ArrayList<Card>();
				passlist.add(passcard);
				trash.push(passlist);
				System.out.println("You Passed.");
				break;
			}else if((isHigherSelection(topCards,selection) || getHandType(topCards).equals("Pass")) && !getHandType(selection).equals("Invalid")) {
				//selection validation
				//put down selection to trash
				trash.push(selection);
				//remove card from hand
				Hand temphand1 = new Hand(currhand);
				for(Card card: selection) temphand1.getHand().remove(card);
				temphand1.sortWeight(temphand1.getHand());
				currhand = temphand1;
				//draw new cards
				currhand.getHand().addAll(currhand.draw(deck, player_id, selection.size()));
				sortWeight(currhand.getHand());
				break;
			}else{
				//failure otherwise
//				Hand temphand = new Hand(currhand);
//				for(Card card: selection) temphand.getHand().add(card);
//				temphand.sortWeight(temphand.getHand());
//				currhand = temphand;
				System.out.println("Invalid selection. Please try again.");
				this.displayStack();
				selection = currhand.select(player_id);
			}
		}
		
		
//		//push first round of cards
//		//case of first player in first turn
//		
//		if(player_num == 1 && turn_num == 1){
//			while(true){
//
//			}
//		}
//		//check if this turn is passed
//		if(selection.isEmpty() && !(player_num == 1 && turn_num == 1)){
//			Card passcard = new Card(player_num,-1,turn_num);
//			List<Card> passlist = new ArrayList<Card>();
//			passlist.add(passcard);
//			trash.push(passlist);
//			System.out.println("You Passed.");
//		}else{
//			//first sort the selection cards
//			sortWeight(selection);
////			if(trash.isEmpty()){
////				trash.push(selection);
////				currhand.getHand().addAll(currhand.draw(deck, player_id, selection.size()));
////				sortWeight(currhand.getHand());
////			}else{
//				while(true){
//					sortWeight(selection);					
//					List<Card> topCards = trash.peek();
//					
//					//compare selection cards with topCards
//
//					if((isHigherSelection(topCards,selection) || getHandType(topCards).equals("Pass")) && !getHandType(selection).equals("Invalid") && !(player_num == 1 && turn_num == 1)){
//						trash.push(selection);
//						currhand.getHand().addAll(currhand.draw(deck, player_id, selection.size()));
//						sortWeight(currhand.getHand());
//						break;
//					}
//					else if(player_num == 1 && turn_num == 1)
//					{
//					    if(selection.contains(currhand.getHand().get(0)) || !getHandType(selection).equals("Invalid"))
//					}
//					
//					}
//					else if(selection.isEmpty()){
//						Card passcard = new Card(player_num,-1,turn_num);
//						List<Card> passlist = new ArrayList<Card>();
//						passlist.add(passcard);
//						trash.push(passlist);
//						System.out.println("You Passed.");
//						break;
//					}else{
//						Hand temphand = new Hand(currhand);
//						for(Card card: selection) temphand.getHand().add(card);
//						temphand.sortWeight(temphand.getHand());
//						currhand = temphand;
////						currhand.displayHand(player_id);
////						displaySelection(selection);
//						System.out.println("Invalid selection. Please try again.");
//						this.displayStack();
//						selection = currhand.select(player_id);
//						
//					}
//				}
////			}
//		}
		

	}
	public void displaySelection(List<Card> selection) {
		String tempString = "Current Selection: [ ";
		for(int i=0 ;i<selection.size();i++){ 
			
			
			tempString += selection.get(i).getStringValue() + "" + selection.get(i).getStringSuit() + " "; 
		}
		tempString += "]";
		System.out.println(tempString);
	}
	public boolean isHigherRank(Card card1, Card card2){
		if(card1.getRank() < card2.getRank()){
			//card2 is higher than card1
			return true;
		}else{
			return false;
		}
	}
	public boolean isHigherSelection(List<Card> topCards,List<Card> selection){
		//compare which cards are higher weight
		
		//bomb case
		if(getHandType(selection).equals("Bomb")){
			if(getHandType(selection).equals(getHandType(topCards))){
				if(topCards.get(0).getRank() < selection.get(0).getRank()){
					return true;
				}else{
					return false;
				}
			}else{
				return true;
			}
		}
		//other cases, check if hand types match
		if(getHandType(selection).equals(getHandType(topCards)))
		{
			switch (getHandType(selection)){
				case "Single":
					return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
				case "Pair":
					return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
				case "Triple":
					return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
				case "FullHouse":
					HashMap<Integer,Integer> tempmap = getHandMap(selection);
					int selection_num = 0;
					Integer[] keyArray = tempmap.keySet().toArray(new Integer[2]);
					Integer[] valueArray = tempmap.values().toArray(new Integer[2]);
					for(int i = 0;i<2;i++){
						if(valueArray[0] == 3){
							selection_num = keyArray[0];
						}
						if(valueArray[1] == 3){
							selection_num = keyArray[1];
	
						}
					}
					tempmap = getHandMap(topCards);
					int top_num = 0;
					keyArray = tempmap.keySet().toArray(new Integer[2]);
					valueArray = tempmap.values().toArray(new Integer[2]);
					for(int i = 0;i<2;i++){
						if(valueArray[0] == 3){
							top_num = keyArray[0];
						}
						if(valueArray[1] == 3){
							top_num = keyArray[1];
	
						}
					}
					if(top_num < selection_num){
						return true;
					}else{
						return false;
					}
					
				case "Straight3":
					return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
				case "Straight4":
					return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
				case "Straight5":
					return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
				case "Straight6":
					return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
				case "Straight7":
					return (isHigherRank(topCards.get(topCards.size()-1),selection.get(selection.size()-1)));
				case "Invalid":
					return false;
				default:
					return false;
			}
		}
		
		//no match in hand type
		return false;
	}
	
	public void sortWeight(List<Card> selection){
	    Collections.sort(selection);
	}
	
	
	public String getHandType(List<Card> selection){
		if(selection.size() == 1)
		{
			if(selection.get(0).getSuit() == -1){
				return "Pass";
			}else{
				return "Single";
			}
			
		}
		else if(selection.size() == 2)
		{
			if(isPair(selection)){
				return "Pair";
			}
			else{
				return "Invalid";
			}
		}
		else if(selection.size() == 3)
		{
			if(isTriple(selection)){
				return "Triple";
			}
			else
			{
				if(isStraight(selection))
				{
					return "Straight3";
				}
				
				else{
					return "Invalid";
				}
			}
			
		}
		else if(selection.size() == 4)
		{
			if(isQuad(selection)){
				return "Bomb";
			}
			else
			{
				if(isStraight(selection))
				{
					return "Straight4";
				}
				
				else{
					return "Invalid";
				}
			}
			
		}
		
		else if(selection.size() == 5)
		{
			
			
			if(isFullHouse(selection)){
				return "FullHouse";
			}
			else
			{
				
				if(isStraight(selection))
				{
					return "Straight5";
				}
				
				else{
					return "Invalid";
				}
			}
			
		}
		
		else if(selection.size() == 6)
		{
				
				if(isStraight(selection))
				{
					return "Straight6";
				}
				
				else{
					return "Invalid";
				}
		}
		
		else if(selection.size() == 7)
		{
				if(isStraight(selection))
				{
					return "Straight7";
				}
				
				
				else{
					return "Invalid";
				}
		}
		
		
		else
		{
			return "Invalid";
		}
		
	}
	
	public boolean isStraight(List<Card> selection)
	{   
		int size = selection.size();
		if(selection.get(size-1).getValue() == 15)
		{
			return false;
		}
		else
		{
			for(int i = 0; i<size-1; i++)
			{
				if(selection.get(i).getValue() != (selection.get(i+1).getValue()-1))
				{
					return false;
				}
					
			}
			return true;
		}		
	}
	
	public boolean isPair(List<Card> selection)
	{
		if(selection.get(0).getValue() == selection.get(1).getValue())
		{
			return true;
		}
		return false;
	}
	
	public boolean isTriple(List<Card> selection)
	{
		if(selection.get(0).getValue() == selection.get(1).getValue() && selection.get(1).getValue() == selection.get(2).getValue()){
			return true;
		}
		return false;
	}
	public boolean isQuad(List<Card> selection)
	{
		if(selection.get(0).getValue() == selection.get(1).getValue() && selection.get(1).getValue() == selection.get(2).getValue() && selection.get(2).getValue() == selection.get(3).getValue())
		{
			return true;
		}
		return false;
	}
	public boolean isFullHouse(List<Card> selection){
		
		//int ref_num;
		//int[] temp = new int(4);
		
		HashMap<Integer,Integer> tempRef = getHandMap(selection);
		
		if(tempRef.containsValue(2) && tempRef.containsValue(3))
		{
			return true;
		}
		
		return false;
		
				
	}
	
	public HashMap<Integer, Integer> getHandMap(List<Card> selection){
		HashMap<Integer,Integer> ref = new HashMap<Integer,Integer>();
		for(int i = 0;i<selection.size();i++){
			
			int cardValue = selection.get(i).getValue();
			if(!ref.containsKey(cardValue)){
				ref.put(cardValue,1);
			}else{
				ref.put(cardValue,ref.get(cardValue)+1);
				
			}
		}
		return ref;
	}


}
