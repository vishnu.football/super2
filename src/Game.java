import java.util.*;
//import Hand;
//import Card;
//import Deck;

public class Game {
	// Methods
	
	public static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

	public static void main(String[] args) {
		
		//TEST 1: Create new deck,tabledeck, and hand for 2 players
		
		String p1_id = getSaltString(); //get random string for p1
		String p2_id = getSaltString(); //get random string for p2
		
		
		//*************************
		//**** DECK TESTING *******
		//*************************
		Deck myDeck = new Deck(p1_id,p2_id);
		
		
//		Deck myDeck = new Deck(p1_id, p2_id, "Bomb");
		
		
		
		
		
		//***** END DECK TESTING *******
		
		
		TableDeck myTableDeck = new TableDeck();
		
		//p1 hand
		Hand p1Hand = new Hand(myDeck, p1_id);
		//p2 hand
		Hand p2Hand = new Hand(myDeck, p2_id);
		
		//TEST 2: display both deck and hand, shuffle full deck and display it
//		p1Hand.displayHand(p1_id);
//		p2Hand.displayHand(p2_id);
//		
//		myDeck.displayDeck();
//		myDeck.displayDeck(p1_id);
//		myDeck.displayDeck(p2_id);
//		
//		System.out.println("\n===============SHUFFLING===============\n");
//		myDeck.shuffleDeck();
//		myDeck.shuffleDeck(p1_id);
//		myDeck.shuffleDeck(p2_id);
//		
//		myDeck.displayDeck();
//		myDeck.displayDeck(p1_id);
//		myDeck.displayDeck(p2_id);
		
		//TEST 3: sort ranking of a hand
//		p1Hand.displayHand(p1_id);
//		
//		System.out.println("\n===============SORTING===============\n");
//		myDeck.sortWeight(p1Hand.getHand());
//		
//		p1Hand.displayHand(p1_id);
//		System.out.println("Done.");
		
		//TEST 4: play a full game
		boolean a_turn = true;
		int turn = 1;
		String A_id,B_id;
		Hand A_hand,B_hand;
		if(p1Hand.getHand().get(0).getRank() < p2Hand.getHand().get(0).getRank()){
			//p1 starts
			A_id = p1_id;
			B_id = p2_id;
			A_hand = p1Hand;
			B_hand = p2Hand;
		}else{
			//p2 starts
			A_id = p2_id;
			B_id = p1_id;
			A_hand = p2Hand;
			B_hand = p1Hand;
		}
		Scanner sc = new Scanner(System.in);
		while(!(A_hand.getHand().isEmpty() || B_hand.getHand().isEmpty())){

			if(a_turn) {
				//Player A turn
				for(int clear = 0; clear < 20; clear++) {
				    System.out.println("");
				}
				
	//			myDeck.displayDeck(A_id);
	//			A_hand.displayHand(A_id);
				System.out.println("Pass to player 1 and press enter to continue: ");
				sc.nextLine();
				
				myTableDeck.displayStack();
				myTableDeck.turn(A_id, A_hand.select(A_id),myDeck,A_hand,1,turn);
				if(A_hand.getHand().isEmpty()) {
					break;
				}
				if(checkDraw(myTableDeck)) {
					break;
				}
				a_turn = false;
			}else {
				//Player B turn
				for(int clear = 0; clear < 20; clear++) {
				    System.out.println("");
				}
				System.out.println("Pass to player 2 and press enter to continue: ");
				sc.nextLine();
				
				myTableDeck.displayStack();
	//			myDeck.displayDeck(B_id);
	//			B_hand.displayHand(B_id);
				
				myTableDeck.turn(B_id, B_hand.select(B_id),myDeck,B_hand,2,turn);
				if(B_hand.getHand().isEmpty()) {
					break;
				}
				if(checkDraw(myTableDeck)) {
					break;
				}
				a_turn = true;
				turn++;
				
			}
				

		}

		if(A_hand.getHand().isEmpty()){
			System.out.println("Player "+A_id+" Won. Game Over.");
		}

		else if(B_hand.getHand().isEmpty()){
			System.out.println("Player "+B_id+" Won. Game Over.");
		}
		else if(checkDraw(myTableDeck)) {
			System.out.println("It's a Draw.");
		}
		else {
			System.out.println("Something went wrong");
		}
		
		
	}
	
	public static boolean checkDraw(TableDeck TD)
	{
		int s = TD.getTrash().size();
		if(s >= 6){
			for(int i=s-6 ; i<s; i++)
			{
				if(TD.getTrash().get(i).get(0).getSuit() != -1)
				{
					return false;
				}
			}
			return true;
		}
		else {
			return false;
		}
		
	}
}
