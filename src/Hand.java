import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

//import Game;
//import Card;
//import Deck;


	public class Hand{
		// Variables:
		private List<Card> hand = new ArrayList<Card>();
		
		// Constructor:
		public Hand(Deck deck,String player_id){
			this.hand = draw(deck,player_id,7);
			sortWeight(this.hand);
		}
		public Hand(Hand hand){
			this.hand = hand.hand;
		}
		// Methods:
		
		public List<Card> select(String player_id){
			int[] intArray;
			List<Card> selection;
			while(true) {
				this.displayHand(player_id);
				selection = new ArrayList<Card>();
				Scanner sc = new Scanner(System.in);
				String inp = "";
				System.out.println("Player "+player_id+" turn. Which cards do you want to put down from your hand this round?");
				System.out.println("Delimit with a space between each card.");
				inp = sc.nextLine();
				
				if(inp.contains("p") || inp.contains("P")){
					return selection;
				}
	
				String[] tempArray = inp.split(" ");
				intArray = new int[tempArray.length];
				if(tempArray == null) {
					return selection;
				}
				for(int i = 0; i<tempArray.length;i++){
					if(tempArray[i].equals("")) {
						return selection;
					}
					intArray[i] = Integer.parseInt(tempArray[i]);
				}
				Arrays.sort(intArray);
				if(intArray[intArray.length-1] > this.hand.size() || intArray[0] < 1) {
					System.out.println("Invalid hand index. Please try again.");
				}else{
					break;
				}
			}
			
			for(int i = intArray.length-1;i>=0;i--){
				selection.add(this.hand.get(intArray[i]-1));
//				this.hand.remove(intArray[i]-1);
			}

			return selection;
		}
		public void sortWeight(List<Card> selection){
		    Collections.sort(selection);
		}
		public List<Card> draw(Deck deck,String player_id, int num_draws){
			List<Card> rethand = new ArrayList<Card>();
			if(deck.isEmpty(player_id)){
				return rethand;
			}
//			deck.displayDeck(player_id);
			for(int i=0;i<num_draws;i++){
				if(deck.isEmpty(player_id)) {
					break;
				}
				//draw num_draws times
				//get deck for player_id deck
				List<Card> tempdeck = deck.getDeck(player_id);
				rethand.add(tempdeck.get(tempdeck.size()-1));
				tempdeck.remove(tempdeck.size()-1);
			}
			
			return rethand;
		}
		// output cards in hand of current player in console
		public void displayHand(String player_id){
			String tempString = "Player "+player_id+" Hand: [ ";
			for(int i=0 ;i<hand.size();i++){ 
				
				
				tempString += hand.get(i).getStringValue() + "" + hand.get(i).getStringSuit() + " "; 
			}
			tempString += "]";
			System.out.println(tempString);
		}
		
		public List<Card> getHand(){
			return this.hand;
		}

		
	}
