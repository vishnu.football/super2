import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import Game;
//import Card;
//import Hand;


	public class Deck{
		// Vars:
		private List<Card> deck;
		private List<Card> p1_deck;
		private List<Card> p2_deck;
		private String id1;
		private String id2;
		private String testCase;
		
		// Constructor:
		public Deck(String id1,String id2){
			this.id1 = id1;
			this.id2 = id2;
			List<Card> cards = new ArrayList<Card>();
			int k = 0;
			for(int i = 3;i<=15;i++){
				for(int j = 1;j<=4;j++){
					Card tempCard = new Card(i,j,k);
					cards.add(tempCard);
					k++;
				}

			}
			for(int i = 0;i<7;i++) {
				Collections.shuffle(cards);	
			}
			
		    List<Card> p1 = new ArrayList<Card>(cards.subList(0, 25));
		    List<Card> p2 = new ArrayList<Card>(cards.subList(26, 51));
		    this.deck = cards;
			this.p1_deck = p1;
			this.p2_deck = p2;
		}
		
		public Deck(String id1, String id2, String testCase)
		{
			this.testCase = testCase;
			this.id1 = id1;
			this.id2 = id2;
			List<Card> cards = new ArrayList<Card>();
			int k = 0;
			for(int i = 3;i<=15;i++){
				for(int j = 1;j<=4;j++){
					Card tempCard = new Card(i,j,k);
					cards.add(tempCard);
					k++;
				}

			}
			//3(1),3(2),3(3),3(4),....15(1),15(2),15(3),15(4)
			
			List<Card> tempP1List = new ArrayList<Card>();
			List<Card> tempP2List = new ArrayList<Card>();
			
			switch(testCase) {
			case "Bomb":
				tempP1List = new ArrayList<>(cards.subList(0, 25));
				tempP2List = new ArrayList<>(cards.subList(26, 51));
				//tempP1List = cards.subList(0, 25);
				//tempP2List = cards.subList(26, 51);
				System.out.println("fullhouse/bomb/triple/pair deck intialized");
				
				break;
			case "Straight":
				
				System.out.println("Single straight 3-7 deck intialized");
				for(int i = 0; i < cards.size()-4; i++)
				{
					if(i%4 == 0)
					{
						System.out.println("Adding one to tempp1list");
						tempP1List.add(cards.get(i));
					}
					else if(i%4 == 1) {
						tempP2List.add(cards.get(i));
					}
				}
				break;
			case "DoubleStraight":
				for(int i = 0; i < cards.size()-4; i++)
				{
					if(i%4 == 0 || i%4 == 3)
					{
						tempP1List.add(cards.get(i));
					}
					else {
						tempP2List.add(cards.get(i));
					}
				}
				System.out.println("Double straight deck initialized");
				break;

			default:
				System.out.println("shuffled deck");
				break;
			}
			
		    
			this.deck = cards;
			this.p1_deck = tempP1List;
			this.p2_deck = tempP2List;
			
		}
		
		// Methods:
		public boolean isEmpty(String player_id) {
			if(player_id.equals(this.id1)){
				return this.p1_deck.isEmpty();
			}else if(player_id.equals(this.id2)){
				return this.p2_deck.isEmpty();
			}else{
				//default case is empty
				return true;
			}
			
		}
		// return deck of input player_id
		public List<Card> getDeck(String player_id){
			if(player_id.equals(this.id1)){
				return p1_deck;
			}else if(player_id.equals(this.id2)){
				return p2_deck;
			}else{
				//default
				return deck;
			}
		}
		
		// display the deck of input id
		public void displayDeck(String player_id){
			List<Card> currdeck;
			if(player_id.equals(id1)){
				currdeck = new ArrayList<Card>(this.p1_deck);
			}else if(player_id.equals(id2)){
				currdeck = new ArrayList<Card>(this.p2_deck);
			}else{
				//default is full deck
				currdeck = new ArrayList<Card>(this.deck);
			}
			String tempString = "Player "+player_id+" Deck: \n [ ";
			for(int i=0 ;i<currdeck.size();i++){ 
				
				tempString += currdeck.get(i).getStringValue() + "" + currdeck.get(i).getStringSuit() + " "; 
			}
			tempString += "]";
			System.out.println(tempString);
		}
		//overloaded displayDeck for full deck display
		public void displayDeck(){
			String tempString = "Full Deck: \n [ ";
			for(int i=0 ;i<deck.size();i++){ 
				
				tempString += deck.get(i).getStringValue() + "" + deck.get(i).getStringSuit() + " "; 
			}
			tempString += "]";
			System.out.println(tempString);
		}
		public void sortWeight(List<Card> selection){
		    Collections.sort(selection);
		}
		// shuffle deck 
		public void shuffleDeck(String player_id){
			if(player_id.equals(id1)){
				Collections.shuffle(p1_deck);
			}else if(player_id.equals(id2)){
				Collections.shuffle(p2_deck);
			}else{
				Collections.shuffle(deck);
				//default is full deck
			}
		}
		public void shuffleDeck(){
			Collections.shuffle(deck);
		}
		
		
		

	}
