import java.util.ArrayList;
import java.util.List;
import java.util.*;

//import Game;
//import Deck;
//import Hand;

public class Card implements Comparable<Card>{
	//fundamental definition of a card
	private int value;
	private int suit;
	private int rank;
	
	// Constructor
	public Card(int value,int suit, int rank){
		this.value = value;
		this.suit = suit;
		this.rank = rank;
	}
	// Methods
	public void setValue(int value){
		this.value = value;
	}
	public void setSuit(int suit){
		this.suit = suit;
	}
	public void setRank(int rank){
		this.rank = rank;
	}
	
	public String getStringValue(){
		switch(this.value){
		case 11:
			return "J";
		case 12:
			return "Q";
		case 13:
			return "K";
		case 14:
			return "A";
		case 15:
			return "2";
		default:
			return ""+this.value;
		}
	
	}
	public String getStringSuit(){
		switch(this.suit){
		case 1:
			return "S";
		case 2:
			return "H";
		case 3: 
			return "C";
		case 4:
			return "D";
		default:
			return "X";
		}
		
	}
	public int getRank(){
		return this.rank;
		
	}
	public int getValue(){
		return this.value;
		
	}
	public int getSuit(){
		return this.suit;
		
	}
	@Override
	public int compareTo(Card compareCard) {
		// TODO Auto-generated method stub
		int compareRank = compareCard.getRank();
		//ascending order
		return this.rank - compareRank;
	}
}