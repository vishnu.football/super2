public class Deck{
    // Vars:
    private List<Card> deck;
    private List<Card> p1_deck;
    private List<Card> p2_deck;
    private String id1;
    private String id2;
    // Constructor:
    public Deck(String id1,String id2){
      this.id1 = id1;
      this.id2 = id2;
      List<Card> cards = new ArrayList<Card>();
      int k = 0;
      for(int i = 3;i<=15;i++){
        for(int j = 1;j<=4;j++){
          Card tempCard = new Card(i,j,k);
          cards.add(tempCard);
         k++;
        }
      }
      Collections.shuffle(cards);  
      
        List<Card> p1 = new ArrayList<Card>(cards.subList(0, 25));
        List<Card> p2 = new ArrayList<Card>(cards.subList(26, 51));
        this.deck = cards;
      this.p1_deck = p1;
      this.p2_deck = p2;
    }
    
        
    // Methods:
    public boolean isEmpty(String player_id) {
      if(player_id.equals(this.id1)){
        return this.p1_deck.isEmpty();
      }else if(player_id.equals(this.id2)){
        return this.p2_deck.isEmpty();
      }else{
        //default case is empty
        return true;
      }
      
    }
    // return deck of input player_id
    public List<Card> getDeck(String player_id){
      if(player_id.equals(this.id1)){
        return p1_deck;
      }else if(player_id.equals(this.id2)){
        return p2_deck;
      }else{
        //default
        return deck;
      }
    }
    
    // display the deck of input id
    
    public void sortWeight(List<Card> selection){
        Collections.sort(selection);
    }
    // shuffle deck 
    public void shuffleDeck(String player_id){
      if(player_id.equals(id1)){
        Collections.shuffle(p1_deck);
      }else if(player_id.equals(id2)){
        Collections.shuffle(p2_deck);
      }else{
        Collections.shuffle(deck);
        //default is full deck
      }
    }
    public void shuffleDeck(){
      Collections.shuffle(deck);
    }

}
