import java.util.*;
Card button;
String A_id,B_id,curr_id;
Hand p1Hand, p2Hand, A_hand,B_hand,theHand,currhand;
TableDeck myTableDeck;
Deck myDeck;
int turn;
//PImage img;

void setup()
{
  size(1500, 1200);
  frameRate(60);

  rectMode(CENTER);
  textAlign(CENTER, CENTER);
  imageMode(CENTER);
  //img = loadImage("back.png");
  //String p1_id = getSaltString();
  //String p2_id = getSaltString();
  String p1_id ="p1";
  String p2_id ="p2";
  myDeck = new Deck(p1_id,p2_id);
  myTableDeck = new TableDeck();
  p1Hand = new Hand(myDeck, p1_id);
  p2Hand = new Hand(myDeck, p2_id);
   turn = 1;
   if(p1Hand.getHand().get(0).getRank() < p2Hand.getHand().get(0).getRank()){
      //p1 starts
      System.out.println("P1 Starts");
      A_id = p1_id;
      B_id = p2_id;
      A_hand = p1Hand;
      B_hand = p2Hand;
    }
    else{
      //p2 starts
      A_id = p2_id;
      B_id = p1_id;
      A_hand = p2Hand;
      B_hand = p1Hand;
    }
    
   button = new Card(19, 0, 1); button.setxPos(4*displayWidth/6); button.setyPos(3*displayHeight/6); button.setWidth(displayWidth/20); button.setHeight(7*displayWidth/100); button.setSpeed(50);

 }

void mouseClicked()
{
  if(mouseY>2*displayHeight/6)
  {
    theHand = A_hand;
  }
  else
  {
    theHand = B_hand;
  }
   for (int i = 0; i < theHand.getHand().size(); i++) {
 if (mouseX > (theHand.getHand().get(i).getxPos() - theHand.getHand().get(i).getWidth() / 2) && mouseX < (theHand.getHand().get(i).getxPos() + theHand.getHand().get(i).getWidth() / 2) && mouseY > (theHand.getHand().get(i).getyPos() - theHand.getHand().get(i).getHeight() / 2) && mouseY < (theHand.getHand().get(i).getyPos() + theHand.getHand().get(i).getHeight() / 2)) {
    if(theHand.getHand().get(i).getClicked()==true)
      {theHand.getHand().get(i).setClicked(false);}
      else
      {theHand.getHand().get(i).setClicked(true);}

     }
   }
  
   if(mouseX > (button.getxPos()-button.getWidth()/2) && mouseX < (button.getxPos() + button.getWidth() / 2) && mouseY > (button.getyPos() - button.getHeight() / 2) && mouseY < (button.getyPos() + button.getHeight() / 2))
   {
      if(button.getClicked()==true)
      {button.setClicked(false);}
      else
      {button.setClicked(true);}
   }
   
}

void draw(){
  if(turn%2==1)
  {curr_id = A_id; currhand = A_hand; background(color(0, 50, 0));}
  else
  {curr_id = B_id; currhand = B_hand; background(color(50, 0, 50));}
  button.display();
  if(!(curr_id==A_id && turn == 1))
   {
  myTableDeck.displayStack();
   }
  A_hand.displayHand("A");
  B_hand.displayHand("B");
  
  if(!(A_hand.getHand().isEmpty() || B_hand.getHand().isEmpty())&&!checkDraw(myTableDeck)){
    if(button.getClicked()==true) //wait until button is clicked
      {
        myTableDeck.turn(curr_id, currhand.select(curr_id),myDeck,currhand,1,turn);
        System.out.println("You Pressed The Button");
             turn++;
                button.setClicked(false);
        }
      }
      
  else
  {
    if(A_hand.getHand().isEmpty()){
      textSize(8*displayWidth/100);
      fill(255,255,255);
      text("Player A Won!", displayWidth/2, displayHeight/2);
    }

    else if(B_hand.getHand().isEmpty()){
      textSize(8*displayWidth/100);
      fill(255,255,255);
      text("Player B Won!", displayWidth/2, displayHeight/2);
    }
    else if(checkDraw(myTableDeck)) {
      textSize(8*displayWidth/100);
      text("You Both Suck Dick!", displayWidth/2, displayHeight/2);
    }
    else {
      System.out.println("Something went wrong");
    }
  }
  }
 


class Card implements Comparable<Card>{
  private float xPos, yPos, width, height, speed;
  private color rgb, rgbstroke;
  private boolean clicked;
  private int value, suit, rank;
  Card(int value, int suit, int rank)
  {
    this.value = value;
    this.suit = suit;
    this.rank = rank;
    this.clicked = false;
  }
  public String getStringValue(){
    switch(this.value){
    case 11:
      return "J";
    case 12:
      return "Q";
    case 13:
      return "K";
    case 14:
      return "A";
    case 15:
      return "2";
    default:
      return ""+this.value;
    }
  
  }
  public String getStringSuit(){
    switch(this.suit){
    case 1:
      return "S";
    case 2:
      return "H";
    case 3: 
      return "C";
    case 4:
      return "D";
    case 0:
      return "Back";
    default:
      return "X";
    }
    
  }
  public int getRank()
  {return this.rank;}
  
  public int getValue()
  {return this.value;}
  
  public int getSuit()
  {return this.suit;}
  
  public float getxPos()
  {return this.xPos;}
  
  public float getyPos()
  {return this.yPos;}
  
  public float getWidth()
  {return this.width;}
  
  public float getHeight()
  {return this.height;}
  
  public float getSpeed()
  {return this.speed;}
  
  public color getRGB()
  {return this.rgb;}
   
  public boolean getClicked()
  {return this.clicked;}

  public void moveX(float s)
  {this.xPos -= s;}
 
  public void setValue(int value){
    this.value = value;
  }
  public void setSuit(int suit){
    this.suit = suit;
  }
  public void setRank(int rank){
    this.rank = rank;
  }
  
  public void setxPos(float xPos)
  {this.xPos = xPos;}
  
  public void setyPos(float yPos)
  {this.yPos = yPos;}
    
  public void setWidth(float width)
  {this.width = width;}
   
  public void setHeight(float height)
  {this.height = height;}
   
  public void setSpeed(float speed)
  {this.speed = speed;}
   
  public void setStroke(color rgbstroke)
  {this.rgbstroke = rgbstroke;}
   
  public void setClicked(boolean click)
  {this.clicked = click;}
  
  @Override
  public int compareTo(Card compareCard) {
    // TODO Auto-generated method stub
    int compareRank = compareCard.getRank();
    //ascending order
    return this.rank - compareRank;
  }
  
  public void display(){

    if(this.suit!=(0))
    {
      fill(color(255,255,255));
      strokeWeight(3); 
      stroke(rgbstroke);
      rect(this.xPos, this.yPos, this.width, this.height);
      textSize(8*width/25);
      fill(rgb);
      if(this.getStringValue().equals("10")) //Special case for 10
      {
            text("I0", xPos-15*width/48, yPos-8*height/20);
            text("I0", xPos+17*width/48, yPos+7*height/20);
      }
      else if(this.getStringValue().equals("J")) //Special case for J
      {
           text(this.getStringValue(), xPos-17*width/48, yPos-8*height/20);
           text(this.getStringValue(), xPos+19*width/48, yPos+13*height/40);
      }
      else if(this.getStringValue().equals("Q")) //Special case for Q
      {
           text(this.getStringValue(), xPos-15*width/48, yPos-8*height/20);
           text(this.getStringValue(), xPos+16*width/48, yPos+13*height/40);
      }
      else{
      text(this.getStringValue(), xPos-17*width/48, yPos-8*height/20);
      text(this.getStringValue(), xPos+9*width/24, yPos+7*height/20);
      }
    }
    else
    {
      this.Back();
    }
    if(this.getStringSuit().equals("H"))
    {this.Heart(); rgb=color(255,0,0);}   
    else if(this.getStringSuit().equals("D"))
    {this.Diamond(); rgb=color(255,0,0); } 
    else if(this.getStringSuit().equals("S"))
    {this.Spade(); rgb=color(0,0,0); } 
    else if(this.getStringSuit().equals("C"))
    {this.Club(); rgb=color(0,0,0); } 
    
  }
   
   void Heart()
   {
      smooth();
      noStroke();
      beginShape();
      fill(rgb);
      vertex(xPos, yPos); 
      bezierVertex(xPos, yPos-2*height/7, xPos+37*width/50, yPos-1*height/7, xPos, yPos+2*height/7); 
      vertex(xPos, yPos+2*height/7); 
      bezierVertex(xPos-37*width/50, yPos-1*height/7,xPos, yPos-2*height/7, xPos, yPos); 
      endShape(); 
   }
   void Diamond()
   {
      noStroke();
      fill(rgb);
      quad(xPos, yPos+2*height/7, xPos+15*width/50, yPos, xPos, yPos-2*height/7, xPos-15*width/50, yPos);
   }
   void Spade()
   {
      smooth();
      noStroke();
      fill(rgb);
      beginShape();
      vertex(xPos, yPos+height/17); 
      bezierVertex(xPos, yPos+2*height/7, xPos+37*width/50, yPos+3*height/14, xPos, yPos-3*height/17); 
      vertex(xPos, yPos-3*height/17); 
      bezierVertex(xPos-37*width/50, yPos+3*height/14, xPos, yPos+2*height/7, xPos, yPos+height/17); 
      endShape(); 
      
      smooth();
     
      beginShape();   
      noStroke();
      fill(rgb);
      vertex(xPos-11*width/50, yPos+2*height/7); 
      bezierVertex(xPos-7*width/50, yPos+2*height/7, xPos, yPos+1*height/7, xPos, yPos ); 
      vertex(xPos, yPos); 
      bezierVertex(xPos, yPos+1*height/7, xPos+7*width/50, yPos+2*height/7, xPos+11*width/50, yPos+2*height/7); 
      vertex(xPos+11*width/50, yPos+2*height/7);
      bezierVertex(xPos,yPos+2*height/7,xPos,yPos+2*height/7,xPos-11*width/50,yPos+2*height/7);      
      endShape();      
   }
   
   void Club()
   {
      smooth();
      noStroke();
      fill(rgb);
      ellipse(xPos-17*width/100,yPos+3*height/56,26*height/100,26*height/100);
      ellipse(xPos+17*width/100,yPos+3*height/56,26*height/100,26*height/100);
      ellipse(xPos,yPos-11*height/100,26*height/100,26*height/100);
      
      smooth();
     
      beginShape();  
      noStroke();
      fill(rgb);
      vertex(xPos-11*width/50, yPos+2*height/7); 
      bezierVertex(xPos-7*width/50, yPos+2*height/7, xPos, yPos+1*height/7, xPos, yPos ); 
      vertex(xPos, yPos); 
      bezierVertex(xPos, yPos+1*height/7, xPos+7*width/50, yPos+2*height/7, xPos+11*width/50, yPos+2*height/7); 
      vertex(xPos+11*width/50, yPos+2*height/7);
      bezierVertex(xPos,yPos+2*height/7,xPos,yPos+2*height/7,xPos-11*width/50,yPos+2*height/7);      
      endShape();      
   }
   
   void Back()
   {
      //fill(rgb); //to be replaced with deck design
      strokeWeight(3); 
      stroke(rgb);
      //image(img,xPos,yPos,displayWidth/20+3,7*displayWidth/100+3);
      fill(color(255,255,255));
      textSize(8*width/25);
      if(!this.getStringValue().equals("0"))
      {
      text(value,xPos,yPos);
      }
   }
}
