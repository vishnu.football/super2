public class Hand{
    // Variables:
    private List<Card> hand = new ArrayList<Card>();
   
    // Constructor:
    public Hand(Deck deck,String player_id){
      this.hand = draw_card(deck,player_id,7);
      sortWeight(this.hand);
    }
    public Hand(Hand hand){
      this.hand = hand.hand;
    }
    // Methods:
    
    public List<Card> select(String player_number){
       List<Card> selection = new ArrayList<Card>();;
      List<Integer> intList = new ArrayList<Integer>();
       this.displayHand(player_number);
       for(int i = 0; i<this.hand.size(); i++)
        {
        if(this.hand.get(i).getClicked()==true)
          {
          intList.add(i);
          }
        }
       Collections.sort(intList);
       for(int i = intList.size()-1; i>=0; i--)
        {
        selection.add(this.hand.get(intList.get(i)));
        }
      return selection;
    }
    public void sortWeight(List<Card> selection){
        Collections.sort(selection);
    }
    public List<Card> draw_card(Deck deck,String player_id, int num_draws){
      List<Card> rethand = new ArrayList<Card>();
      if(deck.isEmpty(player_id)){
        return rethand;
      }
//      deck.displayDeck(player_id);
      for(int i=0;i<num_draws;i++){
        if(deck.isEmpty(player_id)) {
          break;
        }
        //draw num_draws times
        //get deck for player_id deck
        List<Card> tempdeck = deck.getDeck(player_id);
        rethand.add(tempdeck.get(tempdeck.size()-1));
        tempdeck.remove(tempdeck.size()-1);
      }
      
      return rethand;
    }
    // output cards in hand of current player in console
    public void displayHand(String player_number){
     for (int i = 0; i < hand.size(); i++) {
     this.getHand().get(i).setxPos((0.9 + 1.4 * i) * hand.get(i).getWidth());
          this.getHand().get(i).setWidth(displayWidth/20);
          this.getHand().get(i).setHeight(7*displayWidth/100);
          this.getHand().get(i).setSpeed(50);
          if(player_number.equals("A"))
        {
          this.getHand().get(i).setyPos(5*displayHeight/6);
        }
        else
        {
          this.getHand().get(i).setyPos(1*displayHeight/6);
        }
          hand.get(i).display();
          if (mouseX > (hand.get(i).getxPos() - hand.get(i).getWidth() / 2) && mouseX < (hand.get(i).getxPos() + hand.get(i).getWidth() / 2) && mouseY > (hand.get(i).getyPos() - hand.get(i).getHeight() / 2) && mouseY < (hand.get(i).getyPos() + hand.get(i).getHeight() / 2)) {
                      if(hand.get(i).getClicked()==true)
                       {
                         hand.get(i).setStroke(color(0, 0, 128));  //clicked and in region(making selection)
                       }
                       else
                       {
                         hand.get(i).setStroke(color(255,255,0));    //not clicked and in region(hover)
                       }
                  }
                  
                  else {   
                    if(hand.get(i).getClicked()==true)
                       {
                         hand.get(i).setStroke(color(0, 0, 128));  //clicked and not in region(selection preserved)
                       }
                    else
                       {
                         hand.get(i).setStroke(color(255,255,255));  //not clicked and not in region(normal card)
                       }
                  } 
            }
    }
    
    public List<Card> getHand(){
      return this.hand;
    }
  }
